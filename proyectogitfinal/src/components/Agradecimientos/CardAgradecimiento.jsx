export function CardAgradecimiento({ nombre, descripcion }) {
  return (
    <div className='col-3 py-4'>
      <div className='card' style={{ width: '18rem' }}>
        <div className='card-body'>
          <h5 className='card-title'>{nombre}</h5>
          <p className='card-text'>{descripcion}</p>
        </div>
      </div>
    </div>
  );
}
